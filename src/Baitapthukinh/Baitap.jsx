import React, { Component } from 'react';

class Baitap extends Component {

    state = {
        current_glass: {
            "id": 1,
            "price": 30,
            "name": "GUCCI G8850U",
            "url": "./glassesImage/v1.png",
            "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },

        GLASSES: [
            {
                "id": 1,
                "price": 30,
                "name": "GUCCI G8850U",
                "url": "./glassesImage/v1.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 2,
                "price": 50,
                "name": "GUCCI G8759H",
                "url": "./glassesImage/v2.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 3,
                "price": 30,
                "name": "DIOR D6700HQ",
                "url": "./glassesImage/v3.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 4,
                "price": 70,
                "name": "DIOR D6005U",
                "url": "./glassesImage/v4.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 5,
                "price": 40,
                "name": "PRADA P8750",
                "url": "./glassesImage/v5.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 6,
                "price": 60,
                "name": "PRADA P9700",
                "url": "./glassesImage/v6.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 7,
                "price": 80,
                "name": "FENDI F8750",
                "url": "./glassesImage/v7.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 8,
                "price": 100,
                "name": "FENDI F8500",
                "url": "./glassesImage/v8.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 9,
                "price": 60,
                "name": "FENDI F4300",
                "url": "./glassesImage/v9.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            }
        ]

    }

    changeGlass = (glassObj) => {
        this.setState({
            current_glass: glassObj
        })
    }

    renderGlassesList = () => {
        let glassListMapped = this.state.GLASSES.map((glass) => {
            return (
                <div>
                    <img onClick={() => {
                        this.changeGlass(glass)
                    }} src={glass.url} alt="glasses" style={{ width: 120 }} />
                </div>
            )
        })
        return glassListMapped;
    }

    render() {

        return (
            <div>
                <div className="card" style={{ width: '18rem' }}>
                    <div style={{ zIndex: 1000 }}>
                        <img src={this.state.current_glass.url} alt="glasses model" style={{ width: 100, marginBottom: -170, opacity: 0.8 }} />
                    </div>
                    <img className='px-5' src='./glassesImage/model.jpg' alt="model img" style={{ position: "relative" }} />
                    <div className="card-body">
                        <h5 className="card-title">{this.state.current_glass.name}</h5>
                        <p className="card-text">{this.state.current_glass.desc}</p>
                    </div>
                </div>
                <div className='row'>
                    {this.renderGlassesList()}
                </div>
            </div >
        );
    }
}

export default Baitap;
