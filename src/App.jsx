import './App.css';
import Baitap from './Baitapthukinh/Baitap';

function App() {
  return (
    <div className="App" style={{
      backgroundImage: "url(/glassesImage/background.jpg)",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      
    }}>
      <div className='container'>
        <Baitap></Baitap>
      </div>
    </div>
  );
}

export default App;
